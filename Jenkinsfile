/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

#!/usr/bin/groovy
node('maven') {
  def vars

  checkout([$class: 'GitSCM', branches: [[name: '*/$GIT_BRANCH']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: PULL_KEY, url: GIT_URL]]])
  
  container(name: 'maven') {
    stage 'Build Release'
      
    def m = readMavenPom file: 'pom.xml'

    vars = initVariables {
      IMPL_VERSION = m.getVersion()
    }
     
    sh 'mvn versions:set -DnewVersion=' + vars.IMPL_VERSION
    sh 'mvn clean deploy -U'
    
    stage('Preparing resources for docker image...') {      
      sh 'mvn dependency:copy-dependencies'
    }
  }

  container('docker') {
    stage('Creating and deploying docker image...') {      
      sh "docker build -t ${vars.IMAGE_NAME} ."
      sh "docker push ${vars.IMAGE_NAME}"
    }
  }
}
